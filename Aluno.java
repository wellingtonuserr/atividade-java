
public class Aluno{
	// Declarar atributos (propriedades)
	int codigo;
	String nome;
	int idade;

	// Construtor
	public Aluno(){

	}

	//Métodos
	private boolean validarNome(String name){
		boolean res = false;
		if(name.lenght() >= 3){
			res = true;
		}
		else if(name.toUpper() == name.toString()){
			if(name.lenght() > 1){
				res = true;
			}
		}

		return res;
	}

	private boolean validarIdade(int age){
		boolean res = false;
		if(idade > 5){
			res = true;
		}

		return res;
	}

	public int getCodigo(){
		return codigo;
	}

	public int getIdade(){
		return idade;
	}

	public String getNome(){
		return nome;
	}

	protected void setNome(String name) throws Exception{
		if(validarNome(name) == true){
			this.nome = name;
		}
		else{
			throw new NomeInvalidoException();
			
		}
	}

	protected void setIdade(int age) throws Exception{
		if(validarIdade(age) == true){
			this.idade = age;
		}
		else{
			throw new IdadeInvalidoException();
			
		}
	}
}

//
class NomeInvalidoException extends Exception{

}

class IdadeInvalidoException extends Exception{

}