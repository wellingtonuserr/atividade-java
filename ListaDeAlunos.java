public class ListaDeAlunos{
	public static void main(String [] arg){
		try{
			Aluno [] alunos = new Aluno[]{
				new Aluno(1, "Ana", 19),
				new Aluno(2, "Wellington", 19),
				new Aluno(3, "Breno", 20),
				new Aluno(4, "Melissa", 15),
				new Aluno(5, "Zoe", 12),
				new Aluno(6, "Levi", 3)
			};

			for(Aluno pessoa : alunos){
				System.out.println(pessoa);
			}
		}
		catch(Exception ex){
			System.out.println(ex);
		}
	}
}